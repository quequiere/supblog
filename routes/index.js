const jwt = require('jsonwebtoken');
const cors = require('cors');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const nodemailer = require('nodemailer');
const router = require('express').Router();

const User = require('../models/User');
const Article = require('../models/Article');
const Comment = require('../models/Comment');
const EmailToken = require('../models/EmailToken');
const ObjectId = require('mongodb').ObjectId;
router.use(cors());

process.env.SECRET_KEY = 'secret';

router.get('/', function(req, res) {
  res.render('index', { title: 'Express' });
});

router.post('/register', (req, res) => {
  const today = new Date();
  const userData = {
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
    isAdmin: req.body.isAdmin,
    password: req.body.password,
    created: today
  };

  User.findOne({ email: req.body.email })
      .then(user => {
        if (!user) {
          bcrypt.hash(req.body.password, 10, (err, hash) => {
              userData.password = hash;

              // Token generation for registration email
              const token = {_userId: req.body.email, token: crypto.randomBytes(16).toString('hex')};

              // Save the verification token
              EmailToken.create(token)
                  .then(() => {
                      // Send the email
                      const transporter = nodemailer.createTransport({
                          host: 'smtp.ethereal.email',
                          port: 587,
                          auth: { user: 'kelsie.simonis1@ethereal.email', pass: 'xQGTHrZ8C5XCc6Zkyk' }
                      });
                      const mailOptions = {
                          from: 'no-reply@supblog.com',
                          to: req.body.email,
                          subject: 'Account Verification',
                          text: 'Hello,\n\n' + 'Thanks to verify your account by clicking the link: \nhttp:\/\/' + req.headers.host + '\/confirmation\/' + token.token + '.\n'
                      };

                      transporter.sendMail(mailOptions, function (err) {
                          if (err) {
                              return res.status(500).send({msg: err.message});
                          }

                          User.create(userData)
                              .then(user => {
                                  res.json({status: user.email + ' registered. A verification email has been sent to your mailbox.'})
                              })
                              .catch(err => {
                                  res.send('error: ' + err)
                              })
                      })
                  })
          })
        } else {
          res.json({ error: 'User already exists' })
        }
      })
      .catch(err => {
        res.send('error: ' + err)
      })
});

router.post('/login', (req, res) => {
  User.findOne({ email: req.body.email })
      .then(user => {
        if (user) {
          if (bcrypt.compareSync(req.body.password, user.password)) {
            const payload = {
              _id: user._id,
              first_name: user.first_name,
              last_name: user.last_name,
              email: user.email,
              isAdmin: user.isAdmin
            };

            let token = jwt.sign(payload, process.env.SECRET_KEY, {
              expiresIn: 1440
            });

            res.send(token)
          } else {
            res.json({ error: 'Password for user do not match our records' })
          }
        } else {
          res.json({ error: 'User does not exist' })
        }
      })
      .catch(err => {
        res.send('error: ' + err)
      })
});

router.post('/article', (req, res) => {
    const today = new Date();
    const articleData = {
        title: req.body.title,
        description: req.body.description,
        hidden: false,
        created: today
    };

    Article.create(articleData)
        .then(() => {
            res.json('Your new article have been posted!')
        })
        .catch(err => {
            res.send('error: ' + err)
        })
});

router.get('/articles', (req, res) => {
    Article.find()
        .then((data) => {
            res.json(data)
        }).catch(err => {
        res.send('error: ' + err)
    })
});

router.post('/articleSingle', (req, res) => {
    const id = req.body.id;
    const objectId = new ObjectId(id);

    Article.findOne( { _id: objectId } )
        .then((data) => {
            res.json(data)
        }).catch(err => {
            res.send('error: ' + err)
    })
});

router.post('/articleDelete', (req, res) => {
    const id = req.body.id;
    const objectId = new ObjectId(id);

    Article.deleteOne( { _id: objectId } )
        .then((data) => {
            console.log(data);
            res.json('Article deleted from database.')
        }).catch(err => {
            res.send('error: ' + err)
    })
});

router.post('/articleModify', (req, res) => {
    const id = req.body.id;
    const objectId = new ObjectId(id);
    const today = new Date();
    const articleData = {
        title: req.body.title,
        description: req.body.description,
        hidden: false,
        created: today
    };

    Article.findByIdAndUpdate(objectId, articleData)
        .then(() => {
            res.json('Your article have been modified!')
        })
        .catch(err => {
            res.send('error: ' + err)
        })
});

router.post('/comments', (req, res) => {
    const articleId = req.body.articleId;

    Comment.find({ articleId: articleId })
        .then((data) => {
            res.json(data)
        }).catch(err => {
            res.send('error: ' + err)
    })
});

module.exports = router;
