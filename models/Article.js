const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ArticleSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    hidden: {
        type: Boolean
    },
    date: {
        type: Date,
        default: Date.now
    }
});

module.exports = User = mongoose.model('articles', ArticleSchema);
