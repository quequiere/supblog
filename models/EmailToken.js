const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const EmailTokenSchema = new Schema({
    _userId: {
        type: String,
        required: true
    },
    token: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now,
        expires: 43200
    }
});

module.exports = User = mongoose.model('emailTokens', EmailTokenSchema);
