import Vue from 'vue'
import App from './App'
import router from './router'
import Toasted from 'vue-toasted'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

const moment = require('moment')
require('moment/locale/fr')

require('../node_modules/bootstrap/dist/css/bootstrap.css')

Vue.config.productionTip = false
Vue.use(Toasted, { position: 'bottom-center', duration: 3000 })
Vue.use(Vuetify)
Vue.use(require('vue-moment'), {
  moment
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
