import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Login from '@/components/Login'
import Register from '@/components/Register'
import Profile from '@/components/Profile'
import CreateArticle from '@/components/CreateArticle'
import Article from '@/components/Article'
import ModifyArticle from '@/components/ModifyArticle'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/create-article',
      name: 'CreateArticle',
      component: CreateArticle
    },
    {
      path: '/article',
      name: 'Article',
      component: Article
    },
    {
      path: '/modify-article',
      name: 'ModifyArticle',
      component: ModifyArticle
    }
  ]
})
