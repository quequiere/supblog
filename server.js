const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const http = require('http');
const path = require('path');
const cookieParser = require('cookie-parser');
const createError = require('http-errors');
const socket = require('socket.io');
const logger = require('morgan');
const cors = require('cors');

const app = express();
const port = process.env.PORT || 3000;
const server = http.createServer(app);
const io = socket(server,{ origins: '*:*'});
const mongoURI = 'mongodb://localhost:27017/server';
const Comment = require('./models/Comment');

app.set('views', path.join(__dirname, 'public'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(cookieParser());
app.use(cors());
app.use(bodyParser.json());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(
    bodyParser.urlencoded({
        extended: true
    })
);

app.use('/api', require('./routes'));

mongoose
    .connect(
        mongoURI,
        { useNewUrlParser: true }
    )
    .then(() => console.log('MongoDB Connected'))
    .catch(err => console.log(err));

io.on('connection',function (sock) {
    console.log('Socket ID: ' + sock.id);

    sock.on('event' , function (data) {
        const today = new Date();

        const commentData = {
            articleId: data.articleId,
            author: data.author,
            message: data.message,
            date: today
        };

        Comment.create(commentData)
            .then(() => {
                io.emit('data', { data });
            })
    })
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

server.listen(port, function() {
    console.log('Server is running on: http://localhost:' + port + '/api')
});
